package com.anatoliy.testapplication

import android.os.Bundle
import android.preference.PreferenceActivity
import androidx.appcompat.app.AppCompatActivity
import com.yandex.mapkit.geometry.Point


class MainActivity : PreferenceActivity() {

    companion object {
        const val API_KEY = "b01db7ac-a21f-4f84-8ce9-b8df58131337"
        const val OBJECT_SIZE = 0.0015

        val pointList = listOf(
            listOf(
                Point(43.232770, 76.947476),
                Point(43.228368, 76.948113)
            ),
            listOf(
                Point(43.237052, 76.942373),
                Point(43.236808, 76.943276),
                Point(43.237162, 76.948138),
                Point(43.237578, 76.948905)
            )
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.main)

    }
}
