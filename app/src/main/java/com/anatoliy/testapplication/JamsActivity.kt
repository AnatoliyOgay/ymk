package com.anatoliy.testapplication

import android.app.Activity
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.mapview.MapView
import com.yandex.mapkit.traffic.TrafficColor
import com.yandex.mapkit.traffic.TrafficLayer
import com.yandex.mapkit.traffic.TrafficLevel
import com.yandex.mapkit.traffic.TrafficListener


class JamsActivity : Activity(), TrafficListener {

    private var levelText: TextView? = null
    private var levelIcon: ImageButton? = null
    private var trafficLevel: TrafficLevel? = null

    private enum class TrafficFreshness {
        Loading,
        OK,
        Expired
    }

    private var trafficFreshness: TrafficFreshness? = null
    private var mapView: MapView? = null
    private var traffic: TrafficLayer? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        MapKitFactory.setApiKey(MainActivity.API_KEY)
        MapKitFactory.initialize(this)
        setContentView(R.layout.jams)
        super.onCreate(savedInstanceState)
        mapView = findViewById(R.id.mapview)
        mapView?.map?.move(
            CameraPosition(
                Point(59.945933, 30.320045),
                14.0f,
                0.0f,
                0.0f
            )
        )
        levelText = findViewById(R.id.traffic_light_text)
        levelIcon = findViewById(R.id.traffic_light)
        traffic = mapView?.mapWindow?.let { MapKitFactory.getInstance().createTrafficLayer(it) }
        traffic?.isTrafficVisible = true
        traffic?.addTrafficListener(this)
        updateLevel()
    }

    override fun onStop() {
        mapView?.onStop()
        MapKitFactory.getInstance().onStop()
        super.onStop()
    }

    override fun onStart() {
        super.onStart()
        MapKitFactory.getInstance().onStart()
        mapView?.onStart()
    }

    private fun updateLevel() {
        val iconId: Int
        var level = ""
        when {
            !traffic!!.isTrafficVisible -> iconId = R.drawable.icon_traffic_light_dark
            trafficFreshness == TrafficFreshness.Loading -> iconId = R.drawable.icon_traffic_light_violet
            trafficFreshness == TrafficFreshness.Expired -> iconId = R.drawable.icon_traffic_light_blue
            trafficLevel == null -> iconId = R.drawable.icon_traffic_light_grey // state is fresh but region has no data
            else -> {
                iconId = when (trafficLevel?.color) {
                    TrafficColor.RED -> R.drawable.icon_traffic_light_red
                    TrafficColor.GREEN -> R.drawable.icon_traffic_light_green
                    TrafficColor.YELLOW -> R.drawable.icon_traffic_light_yellow
                    else -> R.drawable.icon_traffic_light_grey
                }
                level = trafficLevel?.level.toString()
            }
        }
        levelIcon?.setImageBitmap(BitmapFactory.decodeResource(resources, iconId))
        levelText?.text = level
    }

    fun onLightClick(view: View?) {
        traffic?.isTrafficVisible = !traffic!!.isTrafficVisible
        updateLevel()
    }

    fun onClickBack(view: View?) {
        finish()
    }

    override fun onTrafficChanged(trafficLevel: TrafficLevel?) {
        this.trafficLevel = trafficLevel
        trafficFreshness = TrafficFreshness.OK
        updateLevel()
    }

    override fun onTrafficLoading() {
        trafficLevel = null
        trafficFreshness = TrafficFreshness.Loading
        updateLevel()
    }

    override fun onTrafficExpired() {
        trafficLevel = null
        trafficFreshness = TrafficFreshness.Expired
        updateLevel()
    }
}