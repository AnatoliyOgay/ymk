package com.anatoliy.testapplication

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.map.MapType
import com.yandex.mapkit.mapview.MapView
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

/**
 * This is a basic example that displays a map and sets camera focus on the target location.
 * Note: When working on your projects, remember to request the required permissions.
 */
class CustomizationActivity : Activity() {

    private val TARGET_LOCATION = Point(59.945933, 30.320045)
    private var mapView: MapView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        MapKitFactory.setApiKey(MainActivity.API_KEY)
        /**
         * Initialize the library to load required native libraries.
         * It is recommended to initialize the MapKit library in the Activity.onCreate method
         * Initializing in the Application.onCreate method may lead to extra calls and increased battery use.
         */
        MapKitFactory.initialize(this)
        // Now MapView can be created.
        setContentView(R.layout.map)
        super.onCreate(savedInstanceState)
        mapView = findViewById<View>(R.id.mapview) as MapView
        val map = mapView!!.map
        map.mapType = MapType.VECTOR_MAP

        // Apply customization
        try {
            map.setMapStyle(style())
        } catch (e: IOException) {
            Log.e(TAG, "Failed to read customization style", e)
        }

        // And to show what can be done with it, we move the camera to the center of Saint Petersburg.
        map.move(CameraPosition(TARGET_LOCATION, 15.0f, 0.0f, 0.0f))
    }

    @Throws(IOException::class)
    private fun readRawResource(name: String): String {
        val builder = StringBuilder()
        val resourceIdentifier = resources.getIdentifier(name, "raw", packageName)
        val `is` = resources.openRawResource(resourceIdentifier)
        val reader = BufferedReader(InputStreamReader(`is`))
        try {
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                builder.append(line)
            }
        } catch (ex: IOException) {
            Log.e(TAG, "Cannot read raw resource $name")
            throw ex
        } finally {
            reader.close()
        }
        return builder.toString()
    }

    @Throws(IOException::class)
    private fun style(): String = readRawResource("customization_example")

    override fun onStop() {
        // Activity onStop call must be passed to both MapView and MapKit instance.
        mapView?.onStop()
        MapKitFactory.getInstance().onStop()
        super.onStop()
    }

    override fun onStart() {
        // Activity onStart call must be passed to both MapView and MapKit instance.
        super.onStart()
        MapKitFactory.getInstance().onStart()
        mapView?.onStart()
    }

    companion object {
        private const val TAG = "CustomizationActivity"
    }
}