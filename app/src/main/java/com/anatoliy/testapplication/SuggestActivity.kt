package com.anatoliy.testapplication

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import androidx.annotation.NonNull
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.BoundingBox
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.search.SearchFactory
import com.yandex.mapkit.search.SearchManager
import com.yandex.mapkit.search.SearchManagerType
import com.yandex.mapkit.search.SuggestItem
import com.yandex.mapkit.search.SuggestOptions
import com.yandex.mapkit.search.SuggestSession
import com.yandex.mapkit.search.SuggestType
import com.yandex.runtime.Error
import com.yandex.runtime.network.NetworkError
import com.yandex.runtime.network.RemoteError
import java.util.*
import kotlin.math.min

/**
 * This example shows how to request a suggest for search requests.
 */
class SuggestActivity : Activity(), SuggestSession.SuggestListener {
    /**
     * Replace "your_api_key" with a valid developer key.
     * You can get it at the https://developer.tech.yandex.ru/ website.
     */
    private val RESULT_NUMBER_LIMIT = 5
    private var searchManager: SearchManager? = null
    private var suggestSession: SuggestSession? = null
    private var suggestResultView: ListView? = null
    private var resultAdapter: ArrayAdapter<*>? = null
    private var suggestResult: MutableList<String?>? = null
    private val CENTER = Point(55.75, 37.62)
    private val BOX_SIZE = 0.2
    private val BOUNDING_BOX = BoundingBox(
        Point(
            CENTER.latitude - BOX_SIZE,
            CENTER.longitude - BOX_SIZE
        ),
        Point(
            CENTER.latitude + BOX_SIZE,
            CENTER.longitude + BOX_SIZE
        )
    )

    private val SEARCH_OPTIONS: SuggestOptions = SuggestOptions().setSuggestTypes(
        SuggestType.GEO.value or
                SuggestType.BIZ.value or
                SuggestType.TRANSIT.value
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        MapKitFactory.setApiKey(MainActivity.API_KEY)
        MapKitFactory.initialize(this)
        SearchFactory.initialize(this)
        setContentView(R.layout.suggest)
        super.onCreate(savedInstanceState)
        searchManager = SearchFactory.getInstance().createSearchManager(SearchManagerType.COMBINED)
        suggestSession = searchManager?.createSuggestSession()
        val queryEdit = findViewById<View>(R.id.suggest_query) as EditText
        suggestResultView = findViewById<View>(R.id.suggest_result) as ListView
        suggestResult = ArrayList()
        resultAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_list_item_2,
            android.R.id.text1,
            suggestResult as ArrayList<String?>
        )
        suggestResultView?.adapter = resultAdapter
        queryEdit.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(editable: Editable) {
                requestSuggest(editable.toString())
            }
        })
    }

    override fun onStop() {
        MapKitFactory.getInstance().onStop()
        super.onStop()
    }

    override fun onStart() {
        super.onStart()
        MapKitFactory.getInstance().onStart()
    }

    override fun onResponse(@NonNull suggest: List<SuggestItem>) {
        suggestResult?.clear()
        for (i in 0 until min(RESULT_NUMBER_LIMIT, suggest.size)) {
            suggestResult?.add(suggest[i].displayText)
        }
        resultAdapter?.notifyDataSetChanged()
        suggestResultView?.visibility = View.VISIBLE
    }

    override fun onError(@NonNull error: Error) {
        var errorMessage = getString(R.string.unknown_error_message)
        when (error) {
            is RemoteError -> errorMessage = getString(R.string.remote_error_message)
            is NetworkError -> errorMessage = getString(R.string.network_error_message)
        }
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
    }

    private fun requestSuggest(query: String) {
        suggestResultView?.visibility = View.INVISIBLE
        suggestSession?.suggest(query, BOUNDING_BOX, SEARCH_OPTIONS, this)
    }
}