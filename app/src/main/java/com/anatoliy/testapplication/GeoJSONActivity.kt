package com.anatoliy.testapplication

import android.app.Activity
import android.os.Bundle
import android.util.Log
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.RawTile
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.geometry.geo.Projection
import com.yandex.mapkit.geometry.geo.Projections
import com.yandex.mapkit.geometry.geo.XYPoint
import com.yandex.mapkit.layers.LayerOptions
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.map.MapType
import com.yandex.mapkit.mapview.MapView
import com.yandex.mapkit.resource_url_provider.ResourceUrlProvider
import com.yandex.mapkit.tiles.TileProvider
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.*


class GeoJsonActivity : Activity() {

    private val CAMERA_TARGET = Point(59.952, 30.318)
    private val MAX_ZOOM = 30
    private var projection: Projection? = null
    private var urlProvider: ResourceUrlProvider? = null
    private var tileProvider: TileProvider? = null
    private var mapView: MapView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        MapKitFactory.setApiKey(MainActivity.API_KEY)
        MapKitFactory.initialize(this)
        setContentView(R.layout.geo_json)
        super.onCreate(savedInstanceState)
        mapView = findViewById(R.id.mapview)
        mapView?.map?.move(CameraPosition(CAMERA_TARGET, 15f, 0f, 0f))
        mapView?.map?.mapType = MapType.VECTOR_MAP

        // Client code must retain strong references to providers and projection
        projection = Projections.createWgs84Mercator()
        urlProvider = ResourceUrlProvider {
            String.format(
                "https://raw.githubusercontent.com/yandex/mapkit-android-demo/master/src/main/%s", it
            )
        }
        try {
            tileProvider = createTileProvider()
            createGeoJsonLayer()
        } catch (ex: IOException) {
            Log.e(TAG, "Tile provider or GeoJSON layer not created", ex)
        }
    }

    override fun onStop() {
        mapView?.onStop()
        MapKitFactory.getInstance().onStop()
        super.onStop()
    }

    override fun onStart() {
        super.onStart()
        MapKitFactory.getInstance().onStart()
        mapView?.onStart()
    }

    @Throws(IOException::class)
    private fun createGeoJsonLayer() {
        val layer = mapView!!.map.addGeoJSONLayer(
            "geo_json_layer",
            style(),
            LayerOptions(),
            tileProvider!!,
            urlProvider!!,
            projection!!,
            ArrayList()
        )
        layer.invalidate("0")
    }

    @Throws(IOException::class)
    private fun getJsonResource(name: String): String {
        val builder = StringBuilder()
        val resourceIdentifier = resources.getIdentifier(name, "raw", packageName)
        val `is` = resources.openRawResource(resourceIdentifier)
        val reader = BufferedReader(InputStreamReader(`is`))
        try {
            var line: String?
            while (reader.readLine().also { line = it } != null) { builder.append(line) }
        } catch (ex: IOException) {
            reader.close()
            Log.e(TAG, "Cannot read JSON resource $name")
            throw ex
        }
        return builder.toString()
    }

    @Throws(IOException::class)
    private fun style(): String = getJsonResource("geo_json_style_example")

    @Throws(IOException::class)
    private fun createTileProvider(): TileProvider {
        val jsonTemplate = getJsonResource("geo_json_example_template")
        return TileProvider { tileId, version, etag ->
            val tileSize = 1 shl MAX_ZOOM - tileId.z
            val left = tileId.x * tileSize
            val right = left + tileSize
            val bottom = tileId.y * tileSize
            val top = bottom + tileSize
            val leftBottom =
                projection?.xyToWorld(XYPoint(left.toDouble(), bottom.toDouble()), MAX_ZOOM)
            val rightTop =
                projection?.xyToWorld(XYPoint(right.toDouble(), top.toDouble()), MAX_ZOOM)
            val tileLeft = leftBottom!!.longitude
            val tileRight = rightTop!!.longitude
            val tileBottom = leftBottom.latitude
            val tileTop = rightTop.latitude
            val map = HashMap<String, Double>()

            map["@POINT_X@"] = 0.7 * tileLeft + 0.3 * tileRight
            map["@POINT_Y@"] = 0.7 * tileBottom + 0.3 * tileTop
            map["@LINE_X0@"] = 0.9 * tileLeft + 0.1 * tileRight
            map["@LINE_Y0@"] = 0.9 * tileBottom + 0.1 * tileTop
            map["@LINE_X1@"] = 0.9 * tileLeft + 0.1 * tileRight
            map["@LINE_Y1@"] = 0.1 * tileBottom + 0.9 * tileTop
            map["@LINE_X2@"] = 0.1 * tileLeft + 0.9 * tileRight
            map["@LINE_Y2@"] = 0.1 * tileBottom + 0.9 * tileTop
            map["@LINE_X3@"] = 0.1 * tileLeft + 0.9 * tileRight
            map["@LINE_Y3@"] = 0.9 * tileBottom + 0.1 * tileTop
            map["@POLYGON_X0@"] = 0.2 * tileLeft + 0.8 * tileRight
            map["@POLYGON_Y0@"] = 0.8 * tileBottom + 0.2 * tileTop
            map["@POLYGON_X1@"] = 0.5 * tileLeft + 0.5 * tileRight
            map["@POLYGON_Y1@"] = 0.5 * tileBottom + 0.5 * tileTop
            map["@POLYGON_X2@"] = 0.2 * tileLeft + 0.8 * tileRight
            map["@POLYGON_Y2@"] = 0.2 * tileBottom + 0.8 * tileTop
            map["@TEXTURED_POLYGON_X0@"] = 0.8 * tileLeft + 0.2 * tileRight
            map["@TEXTURED_POLYGON_Y0@"] = 0.2 * tileBottom + 0.8 * tileTop
            map["@TEXTURED_POLYGON_X1@"] = 0.2 * tileLeft + 0.8 * tileRight
            map["@TEXTURED_POLYGON_Y1@"] = 0.2 * tileBottom + 0.8 * tileTop
            map["@TEXTURED_POLYGON_X2@"] = 0.5 * tileLeft + 0.5 * tileRight
            map["@TEXTURED_POLYGON_Y2@"] = 0.5 * tileBottom + 0.5 * tileTop

            var json = jsonTemplate
            map.forEach { (key, value) ->
                json = json.replace(key, value.toString())
            }
            RawTile(version, etag, RawTile.State.OK, json.toByteArray())
        }
    }

    companion object {
        private const val TAG = "GeoJsonActivity"
    }
}